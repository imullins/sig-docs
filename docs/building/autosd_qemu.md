# Running AutoSD on qemu

The Automotive SIG publishes images built via [OSBuild](https://www.osbuild.org/)
based on the OSBuild manifest present in the [sample-images](https://gitlab.com/CentOS/automotive/sample-images/)
repository.

Here is a quick guide on how to get you started with them. These steps should work on either Linux or macOS on the x86_64 or AArch64 CPU architectures.

1. Download your qemu image from [https://autosd.sig.centos.org/AutoSD-9/nightly/](https://autosd.sig.centos.org/AutoSD-9/nightly/)
2. Unpack it.

    ```
    unxz auto-osbuild-qemu-cs9-ps-regular-aarch64-602301589.3e284fb7.qcow2.xz
    ```

3. Boot via runvm script from [sample-images](https://gitlab.com/CentOS/automotive/sample-images/) repo:

    ```
    ./runvm auto-osbuild-qemu-cs9-ps-regular-aarch64-602301589.3e284fb7.qcow2
    ```

    1. You might want to add the `--nographics` argument to get a login prompt in the same shell.
    2. If You run it on a different architecture you might need to specify the guest architecture like this: `--arch aarch64`.
4. Login as `root` or `guest` using the password: `password`.
5. Profit

